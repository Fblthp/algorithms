#include <string>
#include <fstream>
#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <sstream>
#include <algorithm>
#include <vector>
#include <queue>
#include <set>

struct edge
{
    int v;
    int c;
    int w;
    int opIdx;
    int flow;

    edge(){};
    edge(int v, int c, int w, int opIdx) : v(v), c(c), w(w), opIdx(opIdx), flow(0){};
};

using namespace std;
vector<vector<edge>> g;
vector<vector<int>> flow;
vector<bool> vis;
set<pair<int, int>> q;
vector<int> dist;
vector<int> p;
vector<int> phi;
vector<int> edgeIdxs;
int n, m, k;

void dijkstra()
{
    q.insert(make_pair(0, 0));
    for (int i = 1; i < n; ++i) 
    {
        q.insert(make_pair((int)1e6 * 2001, i));
        dist[i] = INT_MAX;
    }
   while (!q.empty())
   {
       pair<int, int> cv = *q.begin();
       q.erase(q.begin());
       for (int i = 0; i < g[cv.second].size(); ++i) 
       {
            if (g[cv.second][i].c > 0 && (cv.first + g[cv.second][i].w + 
                (phi[cv.second] - phi[g[cv.second][i].v]) < dist[g[cv.second][i].v]))
            {
                q.erase(make_pair(dist[g[cv.second][i].v], g[cv.second][i].v));
                dist[g[cv.second][i].v] = cv.first + g[cv.second][i].w;
                p[g[cv.second][i].v] = cv.second;
                edgeIdxs[g[cv.second][i].v] = i;
                q.insert(make_pair(dist[g[cv.second][i].v], g[cv.second][i].v));
            }
       }
   }
}

int main()
{
    freopen("input.txt", "r", stdin);
    freopen("output.txt", "w", stdout);
    cin >> n >> m >> k;
    g.resize(n);
    dist.resize(n);
    p.resize(n);
    edgeIdxs.resize(n);
    phi.resize(n);

    for (int i = 0; i < m; ++i) 
    {
        int v1, v2, t;
        cin >> v1 >> v2 >> t;
        v1--;
        v2--;
        g[v1].push_back(edge(v2, 1, t, g[v2].size()));
        g[v2].push_back(edge(v1, 1, t, g[v1].size() - 1));

        g[v1].push_back(edge(v2, 0, -t, g[v2].size() - 1));
        g[v2].push_back(edge(v1, 0, -t, g[v1].size() - 2));
    }

    int tflow = 0;
    int tdist = 0;
    while (tflow < k)
    {
        for (int i = 0; i < p.size(); ++i) p[i] = -1;
        dijkstra();
        if (p[n-1] == -1)
        {
            cout << -1;
            return 0;
        }
        for (int l = 0; l < n; ++l) 
        {
            phi[l] += dist[l];
        }
        int minC = INT_MAX;
        int vert = n - 1;
        tdist += dist[vert];
        while (vert != 0)
        {
            minC = min(minC, g[p[vert]][edgeIdxs[vert]].c);
            vert = p[vert];
        }
        vert = n - 1;
        while (vert != 0)
        {
            g[p[vert]][edgeIdxs[vert]].flow += minC;
            g[p[vert]][edgeIdxs[vert]].c -= minC;

            int opId = g[p[vert]][edgeIdxs[vert]].opIdx;

            g[vert][opId].flow -= minC;
            g[vert][opId].c += minC;

            vert = p[vert];
        }

        tflow = 0;
        for (int j = 0; j < g[0].size(); ++j) 
        {
            tflow += g[0][j].flow;
        }
    }


    cout << tdist / k;
    return 0;
}