#include <string>
#include <fstream>
#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <sstream>
#include <algorithm>
#include <vector>
#include <queue>
 
using namespace std;
vector<vector<int>> v;
vector<vector<int>> flow;
vector<bool> vis; 
queue<int> q;
vector<int> p;
int n;
 
void bfs()
{
    while (!q.empty()) 
    {
        int cv = q.front();
        q.pop();
        if (cv == n + 1) return;
        for (int i = 0; i < v[cv].size(); ++i) 
        {
            if (!vis[i] && v[cv][i] > 0) 
            {
                q.push(i);
                vis[i] = true;
                p[i] = cv;
            }
        }
    }
}
 
int main()
{
    freopen("input.txt", "r", stdin);
    freopen("output.txt", "w", stdout);
    cin >> n;
    v.resize(n + 2);
    flow.resize(n + 2);
    for (int i = 0; i < n + 2; ++i) 
        v[i].resize(n + 2);
    for (int i = 0; i < n + 2; ++i) 
        flow[i].resize(n + 2);
    vis.resize(n + 2);
    p.resize(n + 2);
 
    int t;
    for (int i = 0; i < n; ++i)
    {
        cin >> t;
        if (t == 1) 
            v[0][i + 1] = INT_MAX;
        if (t == 2) 
            v[i + 1][n + 1] = INT_MAX;
    }
 
    for (int i = 0; i < n; ++i) 
        for (int j = 0; j < n; ++j) 
            cin >> v[i + 1][j + 1];
 
    while (1)
    {
        while (!q.empty()) q.pop();
        q.push(0);
        for (int i = 0; i < vis.size(); ++i) 
            vis[i] = false;
        for (int i = 0; i < p.size(); ++i) 
            p[i] = -1;

        bfs();
        if (p[n + 1] == -1) break;

        int minC = INT_MAX;
        int vert = n + 1;
        while (vert != 0)
        {
            minC = min(minC, v[p[vert]][vert]);
            vert = p[vert];
        }
        
        vert = n + 1;
        while (vert != 0)
        {
            flow[p[vert]][vert] += minC;
            flow[vert][p[vert]] -= minC;
 
            v[p[vert]][vert] -= minC;
            v[vert][p[vert]] += minC;
 
            vert = p[vert];
        }
    }
 
    int tflow = 0;
    for (int i = 0; i < flow[0].size(); ++i) 
        tflow += flow[0][i];
 
    cout << tflow;
    return 0;
}