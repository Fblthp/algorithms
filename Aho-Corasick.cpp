#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <vector>
#include <algorithm>
#include <cmath>
#include <stack>
#include <functional>
#include <set>
#include <queue>
#include <string>
#include <map>
#include <iomanip>
#include <cassert>

using namespace std;

#define mp make_pair
#define sqr(x) ((x)*(x))

typedef long long ll;

int n;

struct vertex
{
    int p;
    char edge;
    int nodeLink;
    vector<int> wordIdxs;
    bool terminal;
    bool used;
    int moves[26];

    vertex(int p, char edge) : p(p), edge(edge)
    {
        nodeLink = -1;
        used = false;
        fill(moves, moves + 26, -1);

    };
};

vector<vertex> tree;
vector<bool> ans;
string text;

void addString(string str, int wordIdx)
{
    int curIdx = 0;
    for (int i = 0; i < str.size(); ++i)
    {
        if (tree[curIdx].moves[str[i] - 'a'] != -1)
        {
            curIdx = tree[curIdx].moves[str[i] - 'a'];
        }
        else
        {
            tree.push_back(vertex(curIdx, str[i]));
            tree[curIdx].moves[str[i] - 'a'] = tree.size() - 1;
            curIdx = tree.size() - 1;
        }
    }
    tree[curIdx].terminal = true;
    tree[curIdx].wordIdxs.push_back(wordIdx);
}

int getLink(int v);

int move(int v, int edge)
{
    if (tree[v].moves[edge - 'a'] == -1)
    {
        if (v == 0) tree[v].moves[edge - 'a'] = 0;
        else tree[v].moves[edge - 'a'] = move(getLink(v), edge);
    }
    return tree[v].moves[edge - 'a'];
}

int getLink(int v)
{
    if (tree[v].nodeLink == -1)
    {
        if (v == 0 || tree[v].p == 0) tree[v].nodeLink = 0;
        else tree[v].nodeLink = move(getLink(tree[v].p), tree[v].edge);
    }
    return tree[v].nodeLink;
}

int main()
{
    cin >> n;
    ans.assign(n, 0);
    tree.push_back(vertex(0, '~'));
    tree[0].nodeLink = 0;
    string word;
    for (int i = 0; i < n; ++i) {
        cin >> word;
        addString(word, i);
    }

    cin >> text;
    int curV = 0;
    for (int j = 0; j < text.size(); ++j) {
        curV = move(curV, text[j]);
        int link = curV;
        while (link != 0 && !tree[link].used) {
            tree[link].used = true;
            if (tree[link].terminal)
            {
                for (int i = 0; i < tree[link].wordIdxs.size(); ++i)
                {
                    ans[tree[link].wordIdxs[i]] = 1;
                }
            }
            link = getLink(link);
        }
    }

    for (int l = 0; l < ans.size(); ++l) {
        if (ans[l]) cout << "YES\n";
        else cout << "NO\n";
    }

    return 0;
}

